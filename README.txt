CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Limitations
 * FAQ
 * Maintainers

INTRODUCTION
------------
 * This module tracks the orders placed using mobile(responsive site) and
   displays them in a tabular format.The module also provides a feature to
   export the data.


REQUIREMENTS
------------
This module requires the following modules:

 * Drupal Commerce(https://www.drupal.org/project/commerce)


INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


LIMITATIONS
------------
 * Orders placed using mobile app won't be tracked.The module works only for
   orders placed from responsive site. 
 * Android,iOS,Blackberry,Windows and Symbian are the only supported OS.
 * No details regarding orders placed from browser will be displayed.


FAQ
------------
 Q. Where can I see all the orders and the mobile OS related data?
 A. You can find all the data under Store >> Orders >> Mobile Orders.


MAINTAINER
-----------
Current maintainers:
 * Hardik Pandya (hardik.p) - https://www.drupal.org/user/3220495
